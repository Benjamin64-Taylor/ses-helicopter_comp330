/*
 * SES-Helicopter.cpp for COMP330 2018 Assignment 1
 * Authors: Benjamin Taylor 44856784
 * Date: April 2018
 */

#include <GL/glut.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

void menu(int value);

//Length of time for a single frame
# define FRAME (1000/60)
//Maxiumum movement per frame
# define MAXMOVEMENT 4

struct Globals {
	//Current angle offset of the blades as they spin
	float bladeAngle;
	//Current position of the helicopter. Float for movement calculations
	float curHeliX;
	float curHeliY;
	//Target position of the helicopter
	int targetHeliX;
	int targetHeliY;
	//Mouse positions for movement
	int mouseX;
	int mouseY;
	//If mouse is currently active, i.e. drawing a line for movement
	bool mouseActive;
	//Helicopter is currently spinning/moving to the destination
	bool curMoving;
	//Range: -180:180. The angle that the user has selected the helicopter to move towards
	float targetRot;
	//The current angle that the helicopter is facing
	float curRot;
	//Used to determine if the helicopter is going to rotate left or right to get to the targetRot.
	bool rotLeft;
	//Array used to store all the fires on screen. Each fire posses a x and y co-ordinate.
	int fires[100][2];
	//Following bools to track what state the helicopter is in.
	bool heliLanding;
	bool heliLanded;
	bool heliTaking;
	bool heliFlying;
	//Used for landing/taking off.
	float heliScale;

} globals;

/*
 * Initial checks and calculations. Most importantly assigns random patches around the city.
 * Also prematurely assigns values to mouseX and mouseY in order to remove a bug if the user first clicks without moving the mouse.
 */
void init(void)
{
	glClearColor(0.54, 0.27, 0.075, 0.0); /* set background color to saddlebrown */
	glColor3f(0.0, 0.0, 0.0); /* set drawing color to black */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 1000, 0, 750);
	for(int i = 0; i < 100; i++){
		int degree = rand()%360;
		float theta = degree * M_PI / 180;
		int x = 170 + 150 * cos(theta);
		int y = 230 + 150 * sin(theta);
		globals.fires[i][0] = x;
		globals.fires[i][1] = y;
	}
	globals.mouseX = globals.curHeliX;
	globals.mouseY = globals.curHeliY;
	globals.heliScale = 1;
	globals.heliFlying = true;
}


/*
 * Called when the user presses 'r' or uses the menu to click reset.
 * Resets all variables in the program and restarts it.
 */
void reset() {
	globals.bladeAngle = 0;
	globals.curHeliX = 500;
	globals.curHeliY = 375;
	globals.targetHeliX = 0;
	globals.targetHeliY = 0;
	globals.mouseActive = false;
	globals.curMoving = false;
	globals.targetRot  = 0;
	globals.curRot = 0;
	globals.heliLanding = false;
	globals.heliLanded = false;
	globals.heliScale = 1;
	globals.heliTaking = false;
	globals.heliFlying = true;

	init();
	glutMainLoop();
}
/*
 * Is called whenever a mouse key changes state. Only responds completely to left mouse button.
 * Determines if the helicopter has been clicked and then triggers the flag to follow the mouse position.
 * After letting go of left mouse button, determines how to get to the angle needed for movement.
 * Inputs:
 * 		int Button: The button on the mouse that was pressed, only GLUT_LEFT_BUTTON is expected.
 * 		int state: The current state of the button that was pressed. Expects GLUT_DOWN to set up the tracking but when GLUT_UP occurs the angle calculation occurs.
 * 		int x: The x position of the mouse when a mouse key changes state.
 * 		int y: The y position of the mouse when a mouse key changes state.
 *
 */
void processMouse(int Button,int state, int x, int y) {

	if(Button == GLUT_LEFT_BUTTON) {
		if(globals.heliFlying){
		if (state == GLUT_DOWN) {
			if (x > globals.curHeliX - 30 && x < globals.curHeliX + 30) {
				if(750 -y > globals.curHeliY - 30 && 750 -y < globals.curHeliY + 30){
					globals.mouseX = globals.curHeliX;
					globals.mouseY = globals.curHeliY;
					globals.mouseActive = true;
				}
			}
		}
		else {
			if ((x > globals.curHeliX - 30 && x < globals.curHeliX + 30) && (750 -y > globals.curHeliY - 30 && 750 -y < globals.curHeliY + 30)){
				if (globals.curHeliX > 800 && globals.curHeliX < 928) {
					if(globals.curHeliY > 550 && globals.curHeliY < 731) {
						globals.heliLanding = true;
						globals.targetHeliX = globals.curHeliX;
						globals.targetHeliY = globals.curHeliY;

					}
				}
				}else{
				//Same check as above except opposite. i.e., the mouse has moved drastically.
					globals.targetHeliX = globals.mouseX;
					globals.targetHeliY = globals.mouseY;
					globals.mouseActive = false;
					globals.curMoving = true;
					globals.targetRot = atan2(750 - globals.targetHeliY - globals.curHeliY, globals.targetHeliX - globals.curHeliX) * 180/M_PI;

					if((globals.targetRot) - globals.curRot > 0) {
						globals.rotLeft = true;
					} else {
						globals.rotLeft = false;
					}
				}

			}
		} else {
			globals.heliTaking = true;
		}
	}
}

/*
 * Is called whenever the mouse is moved with a button held. Due to processMouse, this is only when the left button is held.
 * Updates the mouseX & mouseY variables for the line drawn between the helicopter and mouse.
 * Input:
 * 		int x: the x position of the mouse when the function is called
 * 		int y: the y position of the mouse when the function is called
 */
void movingMouse(int x, int y) {
	if (globals.mouseActive == true) {
		globals.mouseX = x;
		globals.mouseY = y;
	}
}



/*
 *Draws the individual blades of the helicopter. All blades are drawn at the same local position of {0,0}.
 */
void drawBlade() {
		glColor3f(0.0,0.0,0.0);
	    glRectf(-3, 0, 3, 50);
}
/*
 *Draws the two blades for the helicopter. bladeAngle is used to determine the current rotation of the blades.
 */
void drawBlades () {
	glPushMatrix();
		glRotatef(0 + globals.bladeAngle, 0.0, 0.0, 1.0);
		drawBlade(); //Top blade
	glPopMatrix();
	glPushMatrix();
		glRotatef(180 + globals.bladeAngle, 0.0, 0.0, 1.0);
		drawBlade(); //Bottom blade
	glPopMatrix();
}
/*
 * Draws all parts of the helicopter with location dependent on the offset. The blades are drawn in their own function.
 *  Inputs:
 *  	float offX: The X location of the helicopter and where it will be drawn for 0.
 * 		float offY: The y location of the helicopter and where it will be drawn for 0.
 */
void drawHelicopter(float offX, float offY) {
	glTranslatef(offX, offY, 0.0);
	glRotatef(globals.curRot, 0.0, 0.0, 1.0); //Starts facing left
	glScalef(globals.heliScale, globals.heliScale, 1);
	glPushMatrix();
		glRotatef(-90, 0.0,0.0,1.0); //Fix rotation from curRot.

		glColor3f(1.0,1.0,0.0); //Yellow
		glRectf(-10, -20, 10, 30); //Main Body

		glColor3f(0.9,0.9,0.9);
		glRectf(-15, -3, 15, 3); //wings

		glColor3f(1.0,0.15,0.15);
		glRectf(-10, -27, 10, -32); //Bottom thick

		glColor3f(1.0,0.0,0.0);
		glRectf(-2, -20, 2, -38); //Bottom thin

		glColor3f(0.0,0.502,0.502);
		glBegin(GL_POLYGON); //Front/cockpit
			glVertex2f(10, 30);
			glVertex2f(5, 38);
			glVertex2f(-5, 38);
			glVertex2f(-10, 30);
		glEnd();
	glPopMatrix();
	glPushMatrix();
		drawBlades();
	glPopMatrix();
}

void drawBuilding(float offX, float offY){
	glTranslatef(offX, offY, 0.0);
	glRectf(0, 0, 60, 60);
}

void drawTown(float offX, float offY) {
	glTranslatef(offX, offY, 0.0);
	glPushMatrix();
		glColor3f(0.82,0.82,0.82); //Grey
		drawBuilding(-30, -30);
	glPopMatrix();
	glPushMatrix();
		glColor3f(1,0.2,0.0); //Orange/red
		drawBuilding(130, 0);
	glPopMatrix();
	glPushMatrix();
		glColor3f(0.6,0.4,0.2); //Camel
		drawBuilding(50, 20);
	glPopMatrix();

}

void drawFire () {
	glColor3f(1.0,0.0,0.0);
	for (int i = 0; i < 100; i++) {
		glRectf(globals.fires[i][0], globals.fires[i][1] ,globals.fires[i][0] + 10, globals.fires[i][1]+10);
	}
}

/*
 * Draws the SES base in the top right corner of the screen. Hard coded.
 */
void drawBase() {
	glColor3f(0.121, 0.121,0.18); //Very dark blue
	glBegin(GL_POLYGON); //HeliPad
		glVertex2f(800, 550);
		glVertex2f(875, 550);
		glVertex2f(928, 603);
		glVertex2f(928, 678);
		glVertex2f(875, 731);
		glVertex2f(800, 731);
		glVertex2f(747, 678);
		glVertex2f(747, 603);
	glEnd();

	//H on the helipad
	glColor3f(1.0,1.0,0.0); //Yellow
	glRectf(800,603, 820, 678);
	glRectf(820, 650, 855, 631);
	glRectf(855, 603, 875, 678);
}
void displayCB(void) /* display callback function,
                        called for initial display and
                        whenever redisplay needed */
{
	glClear( GL_COLOR_BUFFER_BIT); /* clear the screen window */
	glPushMatrix();
		drawTown(100, 200);
	glPopMatrix();
	glPushMatrix();
		drawBase();
	glPopMatrix();
	glPushMatrix();
		drawFire();
	glPopMatrix();
	glPushMatrix();
		drawHelicopter(globals.curHeliX, globals.curHeliY);
	glPopMatrix();
	glPushMatrix();
		if(globals.mouseActive == true){
			glBegin(GL_LINES);
				glVertex2f(globals.curHeliX, globals.curHeliY);
				glVertex2f(globals.mouseX, 750 - globals.mouseY);
			glEnd();
		} else {
			if(globals.curMoving == true){
				glBegin(GL_LINES);
					glVertex2f(globals.curHeliX, globals.curHeliY);
					glVertex2f(globals.targetHeliX, 750 - globals.targetHeliY);
				glEnd();
			}
		}
	glPopMatrix();

	glEnd();
	glFlush(); /* Complete any pending operations */

}
void timer(int y) {
	if(!globals.heliLanded){
		globals.bladeAngle += 20;
	}
	glutTimerFunc(FRAME, timer, 0);
	if(fmod(globals.curRot, 360) != globals.targetRot) {
		if(abs(fmod(globals.curRot,360) - globals.targetRot) < 1) {
			//Increments each angle by 1 at all times, thus decimal angle can't be reached. When less than 1 away, just snap to it
			globals.curRot = globals.targetRot;
		} else{
			if(globals.rotLeft) {
				globals.curRot++;
			} else {
				globals.curRot--;
			}
		}
	} else {
		if (globals.curMoving == true) {
			if(globals.curHeliX != globals.targetHeliX && globals.curHeliY != globals.targetHeliY) {
				float deltaX, deltaY, length;
				deltaX = globals.targetHeliX - globals.curHeliX;
				deltaY = 750- globals.targetHeliY - globals.curHeliY;
				length = sqrt(deltaX * deltaX + deltaY * deltaY);
				if(length > MAXMOVEMENT * 0.1666){
					if(length < 2) {
						globals.curHeliX = globals.targetHeliX;
						globals.curHeliY = 750 - globals.targetHeliY;

					} else {
						deltaX = deltaX/length;
						deltaY = deltaY/length;
						globals.curHeliX += deltaX * MAXMOVEMENT;
						globals.curHeliY += deltaY * MAXMOVEMENT;
				}
			}
			}else {
				globals.curMoving = false;
			}
		}
	}
	if(globals.heliLanding) {
		if(globals.heliScale > 0.6) {
			globals.heliScale -= 0.02;
			globals.heliFlying = false;
		} else {
			globals.heliLanding = false;
			globals.heliLanded= true;
		}
	}
	if(globals.heliTaking) {
			if(globals.heliScale < 1) {
				globals.heliScale += 0.02;
				globals.heliLanded = false;
			} else {
				globals.heliTaking = false;
				globals.heliFlying= true;
			}
		}
	glutPostRedisplay();
}

void keyCB(unsigned char key, int x, int y) /* keyboard callback function,
                                               called on key press */
{
	if (key == 'q')
		exit(0);
	if (key == 'r')
	    reset();
}

void menu(int value){
	switch(value) {
		case 1:
			exit(0);
		case 2:
			reset();
	}
}
int main(int argc, char *argv[]) {
	srand(time(NULL));
	glutInit(&argc, argv); /* initialize GLUT system */
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(1000, 750); /* width=1000pixels height=750pixels */
	glutCreateWindow("SES Helicopter"); /* create screen window */
	glutDisplayFunc(displayCB); /* register display callback function*/
	glutTimerFunc(FRAME, timer, 0);
	glutMouseFunc(processMouse);
	glutMotionFunc(movingMouse);
	glutKeyboardFunc(keyCB); /* register keyboard callback function*/
    init();      /* call init */
    globals.curHeliX = 500;
    globals.curHeliY = 375;

    glutCreateMenu(menu); //Make a menu
    glutAddMenuEntry("q = quit", 1);
    glutAddMenuEntry("r = reset", 2);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
    glutMainLoop(); /* show screen window, call display and
	                   start processing events... */
	/* execution never reaches this point */
	return 0;
}
